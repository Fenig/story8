from django.shortcuts import render
from django.http import JsonResponse

import requests
from .models import Favorites

def index(request):
    favorite_count = len(Favorites.objects.all())
    return render(request, 'story9.html', {'favorite_count' : favorite_count})

def addToFav(request):
    idx = request.GET.get('idx', None)
    is_favorite = Favorites.objects.filter(idx=idx).exists()
    
    if not is_favorite:
        Favorites.objects.create(idx = idx)

    data = {
    'is_favorite': is_favorite,
    'favorite_count': len(Favorites.objects.all())
    }
    return JsonResponse(data)

def checkFav(request):
    obj = Favorites.objects.all()
    ret = '['
    for i in obj:
        ret += '"' + str(i.idx) + '",'
    ret = ret[:-1]
    ret += ']'
    data = {
    'btn_ids': ret
    }
    return JsonResponse(data)

def reqAPI(request, slug):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + str(slug)
    res = requests.get(url).json()
    return JsonResponse(res)

