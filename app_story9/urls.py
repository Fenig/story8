from django.urls import path
from .views import index, addToFav, checkFav, reqAPI
urlpatterns = [
    path('', index, name='index'),
    path('addToFav/', addToFav, name="addToFav"),
    path('checkFav/', checkFav, name="checkFav"),
    path('reqApi/<slug:slug>/', reqAPI, name="reqAPI")
]