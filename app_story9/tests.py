from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest, JsonResponse

import requests

from .views import index, reqAPI, addToFav, checkFav
from .models import Favorites
# Create your tests here.
class UnitTest(TestCase):
    def test_page_using_correct_template(self):
        response = Client().get(reverse('index'))
        self.assertTemplateUsed(response, 'story9.html')

    def test_index_url_is_exist(self):
        response = Client().get(reverse('index'))
        self.assertEqual(response.status_code,200)

    def test_reqApi_url_is_exist(self):
        response = Client().get(reverse('reqAPI', args=['quilting']))
        self.assertEqual(response.status_code,200)
    
    def test_checkFav_url_is_exist(self):
        response = Client().get(reverse('checkFav'))
        self.assertEqual(response.status_code,200)

    def test_page_using_index_func(self):
        found = resolve(reverse('index'))
        self.assertEqual(found.func, index)
    
    def test_page_using_reqApi_func(self):
        found = resolve(reverse('reqAPI', args=['quilting']))
        self.assertEqual(found.func, reqAPI)

    def test_page_using_addToFav_func(self):
        found = resolve(reverse('addToFav'))
        self.assertEqual(found.func, addToFav)
    
    def test_page_using_checkFav_func(self):
        found = resolve(reverse('checkFav'))
        self.assertEqual(found.func, checkFav)
    
    def test_model_can_create_new_obj(self):
        new_fav = Favorites.objects.create(idx='asasasas')
        count_all_available = Favorites.objects.all().count()
        self.assertEqual(count_all_available, 1)
    
    def test_reqAPI_is_not_None(self):
        url = 'https://www.googleapis.com/books/v1/volumes?q=quilting'
        data = requests.get(url)
        data = data.json()
        self.assertNotEqual(data, None)
