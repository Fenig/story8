"""Story8 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.conf.urls.static import static
from app_story11.views import login, home, logout,search, addFav
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('story9/', include('app_story9.urls')),

    path('', login, name='story11_index'),
    path('addFav/', addFav, name='story11_addFav'),
    path('search/<slug:slug>/', search, name='story11_reqbook'),
    url(r'^login/$', login, name='story11_login'),
    url(r'^logout/$', logout, name='story11_logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    url(r'^home/', home, name='story11_home'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
