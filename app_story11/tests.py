from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest, JsonResponse

from .views import search, addFav
import requests

class UnitTest(TestCase):
    def test_get_success(self):
        response = Client().get(reverse('story11_index'))
        self.assertEqual(response.status_code, 200)
        
    def test_get_success2(self):
        response = Client().get(reverse('story11_login'))
        self.assertEqual(response.status_code, 200)
    
    def test_get_success3(self):
        response = Client().get(reverse('story11_home'))
        self.assertEqual(response.status_code, 200)
    
    def test_page_using_correct_template(self):
        response = Client().get(reverse('story11_index'))
        self.assertTemplateUsed(response, 'login.html')
    
    def test_page_using_correct_template2(self):
        response = Client().get(reverse('story11_home'))
        self.assertTemplateUsed(response, 'home.html')

    def test_search_function(self):
        found = resolve(reverse('story11_reqbook', args=['quilting']))
        self.assertEqual(found.func, search)
    
    def test_search_function_OK(self):
        found = Client().get(reverse('story11_reqbook', args=['quilting']))
        self.assertEqual(found.status_code, 200)
    
    def test_add_fav_function(self):
        found = resolve(reverse('story11_addFav'))
        self.assertEqual(found.func, addFav)
    


class LoginLogoutUnitTest(TestCase):
    def test_login_success(self):
        response = Client().get(reverse('story11_login'))
        self.assertEqual(response.status_code, 200)
    def test_logout_success(self):
        response = Client().get(reverse('story11_logout'))
        self.assertEqual(response.status_code, 302)

