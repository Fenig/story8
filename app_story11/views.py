from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.contrib.auth import logout as logout_user
from django.views.decorators.csrf import csrf_exempt
import requests
# Create your views here.
def login(request):
    print("masuk login")
    return render(request, 'login.html')

def logout(request):
    logout_user(request)
    return redirect('story11_index')

def home(request):
    print("masuk home")
    return render(request, 'home.html')

def search(request, slug):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + str(slug)
    res = requests.get(url).json()
    count = 0
    for i, book in enumerate(res['items']):
        favorited = request.session.get(book['id'], False)
        if favorited:
            res['items'][i]['favorited'] = True
            count += 1
        else: res['items'][i]['favorited'] = False
    res['fav_count'] = count
    request.session["count"] = count
    return JsonResponse(res)

@csrf_exempt
def addFav (request):
    idx = request.GET.get('idx', None)
    isFav = request.GET.get('isFav', None)
    res = {}
    try:
        request.session[idx] = isFav
        request.session["count"] += 1
    except: pass
    res['fav_count'] = request.session["count"]
    return JsonResponse(res)