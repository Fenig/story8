var theme = 1;
// Fungsi untuk mengubah theme
function changeTheme(){
    if(theme == 1) {
        theme = 0;
        $(".ui-accordion-header").each(function() {
            $(this).css("background-color","yellow");
          });
          
        $(".ui-accordion-content").css("color", "rgb(140, 200, 140)");
        $("body").css("background-color", "rgba(227, 240, 106, 1)");
    } else {
        theme = 1;
        $(".ui-accordion-header").each(function() {
            $(this).css("background-color", "orange");
        });
        
        $(".ui-accordion-content").css("color", "rgb(160, 160, 160)");
        $("body").css("background-color", "rgba(227, 240, 106, 0.534)");
    }
}
// fungsi untuk menunggu
function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
      end = new Date().getTime();
   }
 }


$loading = $("loading");
$(document).ready(function(){
    $("#loading").show();
    wait(2000);
    $("#loading").hide();
    $("#aktivitas").accordion();
    $("#pengalaman").accordion();
    $("#prestasi").accordion();
    $("#change-theme").click(changeTheme);
});