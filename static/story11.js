function addToFavorite(idx) {
    $.ajax({
        url: '/addFav/',
        data: {
          'idx': idx,
          'isFav': true,
        },
        dataType: 'json',
        success: function (data) {
            $("#bintang").html("Favorite count: " + data.fav_count);
            alert("It has been added to favorite list!")
            $('#'+idx).hide();
          }
      });

}

$(() => {
    $("#bukuBtn").click(function() {
        let x = "/search/".concat($("#search").val());
        $.ajax({
            url: x,
            success: (result) => {
                let itemSize = result.items.length;
                let res = `<tr>
                <th>No.</th>
                <th>Title</th>
                <th>Description</th>
            </tr>`;
                var i;
                for(i = 0; i < itemSize; i++) {
                    res += '<tr><td>' + (i+1).toString() + '</td>' + '<td>' + result.items[i].volumeInfo.title + '</td>' + '<td>' + result.items[i].volumeInfo.description + '</td>';
                    res += '<td><button id="'+result.items[i].id+'" onClick=\'addToFavorite("'+ result.items[i].id +'")\'>make Favorite</button><td></tr>';
                }
                $("#tabel").html(res);
                $("button").addClass("btn btn-primary");
                $("#tabel").addClass("table-bordered");
                $("#bintang").html("Favorite count: " + String(result.fav_count));
            }
            
        });
    });
});