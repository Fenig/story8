var bintang = document.getElementById("bintang");

function addToFavorite(idx) {
    $.ajax({
        url: '/story9/addToFav/',
        data: {
          'idx': idx
        },
        dataType: 'json',
        success: function (data) {
          if (data.is_favorite) {
            alert("It is already in favorite list!");
            
          } else {
            $("#bintang").html("Favorite count: " + String( data.favorite_count));
            alert("It has been added to favorite list!")
            $('#'+idx).removeClass();
            $('#'+idx).addClass("btn");
          }
        }
      });
}
$(() => {
    $("#bukuBtn").click(function() {
        let x = "/story9/reqApi/".concat($("#search").val());
        $.ajax({
            url: x,
            success: (result) => {
                let itemSize = result.items.length;
                let res = `<tr>
                <th>No.</th>
                <th>Title</th>
                <th>Description</th>
            </tr>`;
                var i;
                for(i = 0; i < itemSize; i++) {
                    res += '<tr><td>' + (i+1).toString() + '</td>' + '<td>' + result.items[i].volumeInfo.title + '</td>' + '<td>' + result.items[i].volumeInfo.description + '</td>';
                    res += '<td><button id="'+result.items[i].id+'" onClick=\'addToFavorite("'+ result.items[i].id +'")\'>make Favorite</button><td></tr>';
                }
                $("#tabel").html(res);
                $("button").addClass("btn btn-primary");
                $("#tabel").addClass("table-bordered");
                $.ajax({
                    url: 'http://localhost:8000/story9/checkFav/',
                    success: (result) => {
                        resultx = JSON.parse(result.btn_ids);
                        for(var x in resultx) {
                            $('#'+resultx[x]).removeClass();
                            $('#'+resultx[x]).addClass("btn");
                        }
                    }
                });
            }
            
        });
    });
});